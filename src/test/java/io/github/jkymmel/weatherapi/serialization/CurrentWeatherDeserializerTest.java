package io.github.jkymmel.weatherapi.serialization;

import com.google.gson.GsonBuilder;
import io.github.jkymmel.weatherapi.model.CurrentWeather;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CurrentWeatherDeserializerTest {

    private GsonBuilder gsonBuilder;

    @Before
    public void setUp() throws Exception {
        gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(CurrentWeather.class, new CurrentWeatherDeserializer());
    }

    @Test
    public void testCurrentWeatherCoordinates() {
        String json = "{\"coord\":{\"lon\":24.75,\"lat\":59.44},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"base\":\"stations\",\"main\":{\"temp\":20,\"pressure\":1020,\"humidity\":100,\"temp_min\":19,\"temp_max\":21},\"visibility\":10000,\"wind\":{\"speed\":7.2,\"deg\":190},\"clouds\":{\"all\":90},\"dt\":1234567890,\"sys\":{\"type\":1,\"id\":5014,\"message\":0.0024,\"country\":\"EE\",\"sunrise\":1234567890,\"sunset\":124567890},\"id\":590447,\"name\":\"Tallinn\",\"cod\":200}\n";
        CurrentWeather currentWeather = gsonBuilder.create().fromJson(json, CurrentWeather.class);
        assertEquals(24.75d, currentWeather.getCoordinates().getLongitude(), 0.001);
        assertEquals(59.44d, currentWeather.getCoordinates().getLatitude(), 0.001);
    }

    @Test
    public void testCurrentWeatherTemperature() {
        String json = "{\"coord\":{\"lon\":24.75,\"lat\":59.44},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"base\":\"stations\",\"main\":{\"temp\":20,\"pressure\":1020,\"humidity\":100,\"temp_min\":19,\"temp_max\":21},\"visibility\":10000,\"wind\":{\"speed\":7.2,\"deg\":190},\"clouds\":{\"all\":90},\"dt\":1234567890,\"sys\":{\"type\":1,\"id\":5014,\"message\":0.0024,\"country\":\"EE\",\"sunrise\":1234567890,\"sunset\":124567890},\"id\":590447,\"name\":\"Tallinn\",\"cod\":200}\n";
        CurrentWeather currentWeather = gsonBuilder.create().fromJson(json, CurrentWeather.class);
        assertEquals(BigDecimal.valueOf(20), currentWeather.getTemperature());
    }

}