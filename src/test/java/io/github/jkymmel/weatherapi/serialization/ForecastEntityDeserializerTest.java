package io.github.jkymmel.weatherapi.serialization;

import com.google.gson.GsonBuilder;
import io.github.jkymmel.weatherapi.model.ForecastEntity;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class ForecastEntityDeserializerTest {

    private GsonBuilder gsonBuilder;

    @Before
    public void setUp() throws Exception {
        gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ForecastEntity.class, new ForecastEntityDeserializer());
    }

    @Test
    public void testNormalForecastTemperature() {
        String json = "{\"dt\":0,\"main\":{\"temp\":25.6,\"temp_min\":17.2,\"temp_max\":29.6,\"pressure\":1027.31,\"sea_level\":1030.84,\"grnd_level\":1027.31,\"humidity\":100,\"temp_kf\":0.24},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10n\"}],\"clouds\":{\"all\":92},\"wind\":{\"speed\":10.12,\"deg\":212.008},\"rain\":{\"3h\":1.335},\"snow\":{},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"1970-00-00 00:00:00\"}";
        ForecastEntity forecastEntity = gsonBuilder.create().fromJson(json, ForecastEntity.class);
        assertEquals(new BigDecimal("25.6"), forecastEntity.getTemperature());
    }

    @Test
    public void testNormalForecastDate() {
        String json = "{\"dt\":0,\"main\":{\"temp\":25.6,\"temp_min\":17.2,\"temp_max\":29.6,\"pressure\":1027.31,\"sea_level\":1030.84,\"grnd_level\":1027.31,\"humidity\":100,\"temp_kf\":0.24},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10n\"}],\"clouds\":{\"all\":92},\"wind\":{\"speed\":10.12,\"deg\":212.008},\"rain\":{\"3h\":1.335},\"snow\":{},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"1970-00-00 00:00:00\"}";
        ForecastEntity forecastEntity = gsonBuilder.create().fromJson(json, ForecastEntity.class);
        assertEquals(LocalDate.ofEpochDay(0), forecastEntity.getDate());
    }
}