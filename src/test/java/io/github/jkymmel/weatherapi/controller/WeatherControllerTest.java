package io.github.jkymmel.weatherapi.controller;

import io.github.jkymmel.weatherapi.model.WeatherReport;
import io.github.jkymmel.weatherapi.service.IOService;
import io.github.jkymmel.weatherapi.service.OpenWeatherMapService;
import io.github.jkymmel.weatherapi.util.WeatherReportCompiler;
import org.junit.Before;

class WeatherControllerTest {

    private WeatherController weatherController;

    private OpenWeatherMapService openWeatherMapService;

    private WeatherReportCompiler weatherReportCompiler;

    private IOService ioService;

    @Before
    void setUp() {
        openWeatherMapService = new OpenWeatherMapService();
        ioService = new IOService();
        weatherReportCompiler= new WeatherReportCompiler();
        weatherController = new WeatherController(openWeatherMapService, ioService, weatherReportCompiler);
    }

}