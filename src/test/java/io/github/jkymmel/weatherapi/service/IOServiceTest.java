package io.github.jkymmel.weatherapi.service;

import io.github.jkymmel.weatherapi.model.Coordinates;
import io.github.jkymmel.weatherapi.model.DayForecast;
import io.github.jkymmel.weatherapi.model.WeatherReport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Files.class, IOService.class})
public class IOServiceTest {

    private IOService ioService;

    @Before
    public void setUp() {
        this.ioService = new IOService();
        System.out.println(ioService);
    }

    @Test
    public void testGetNormalCities() throws IOException {
        List<String> expected = Arrays.asList("Tallinn", "Tartu", "Pärnu");
        PowerMockito.mockStatic(Files.class);
        PowerMockito.when(Files.readAllLines(any())).thenReturn(expected);
        assertArrayEquals(expected.toArray(), ioService.getCities().toArray());
    }

    @Test
    public void testGetNoCities() throws IOException {
        List<String> expected = new ArrayList<>();
        PowerMockito.mockStatic(Files.class);
        PowerMockito.when(Files.readAllLines(any())).thenReturn(expected);
        assertArrayEquals(expected.toArray(), ioService.getCities().toArray());
    }

    @Test
    public void testExportCity() {
        PowerMockito.mockStatic(Files.class);
        List<WeatherReport> reports = new ArrayList<>();
        List<DayForecast> forecasts = new ArrayList<>();
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        WeatherReport weatherReport = new WeatherReport(
                "Best city EUNE äöõü\uE01D",
                new Coordinates(10.0d, 12.0d),
                BigDecimal.TEN,
                forecasts
        );
        reports.add(weatherReport);
        ioService.exportCities(reports);
        PowerMockito.verifyStatic(Files.class, times(1));
    }

    @Test
    public void testExportCities() {
        PowerMockito.mockStatic(Files.class);
        List<WeatherReport> reports = new ArrayList<>();
        List<DayForecast> forecasts = new ArrayList<>();
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        forecasts.add(new DayForecast(BigDecimal.ONE, BigDecimal.ZERO, LocalDate.MAX));
        WeatherReport weatherReport = new WeatherReport(
                "Best city EUNE äöõü\uE01D",
                new Coordinates(10.0d, 12.0d),
                BigDecimal.TEN,
                forecasts
        );
        reports.add(weatherReport);
        reports.add(weatherReport);
        reports.add(weatherReport);
        ioService.exportCities(reports);
        PowerMockito.verifyStatic(Files.class, times(3));
    }
}