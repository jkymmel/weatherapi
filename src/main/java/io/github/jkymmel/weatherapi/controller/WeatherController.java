package io.github.jkymmel.weatherapi.controller;

import io.github.jkymmel.weatherapi.model.WeatherReport;
import io.github.jkymmel.weatherapi.service.IOService;
import io.github.jkymmel.weatherapi.service.OpenWeatherMapService;
import io.github.jkymmel.weatherapi.util.WeatherReportCompiler;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class WeatherController {

    private OpenWeatherMapService openWeatherMapService;

    private IOService ioService;

    private WeatherReportCompiler weatherReportCompiler;

    public WeatherController(OpenWeatherMapService openWeatherMapService, IOService ioService, WeatherReportCompiler weatherReportCompiler) {
        this.openWeatherMapService = openWeatherMapService;
        this.ioService = ioService;
        this.weatherReportCompiler = weatherReportCompiler;
    }

    public void run() throws IOException {
        List<String> cities = ioService.getCities();
        List<WeatherReport> weatherReports = cities.stream()
                .map(city -> {
                    try {
                        return weatherReportCompiler.compile(city,
                                openWeatherMapService.getCurrentWeather(city),
                                openWeatherMapService.getForecast(city));
                    } catch (IOException e) {
                        System.err.println("Failed to get weather for " + city);
                    }
                    return null;
                }).collect(Collectors.toList());
        ioService.exportCities(weatherReports);
    }
}
