package io.github.jkymmel.weatherapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class ForecastEntity {
    private BigDecimal temperature;
    private LocalDate date;
}
