package io.github.jkymmel.weatherapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
public class WeatherReport {
    private String city;
    private Coordinates coordinates;
    private BigDecimal temperature;
    private List<DayForecast> dayForecasts;

    public String getPrintableString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("=== " + city + " ===\n");
        stringBuilder.append("Coordinates: " + coordinates.getLatitude() + ", " + coordinates.getLongitude());
        stringBuilder.append("\nTemperature: " + temperature.toString() + "°C\n");
        dayForecasts.forEach(dayForecast -> stringBuilder.append("Date:" + dayForecast.getDate().toString()
                + "\t\t Max temperature: " + dayForecast.getMaxTemperature().toString() + "°C"
                + "\t\t Min temperature: " + dayForecast.getMinTemperature().toString() + "°C\n"));
        stringBuilder.append("\n\n");
        return stringBuilder.toString();
    }
}
