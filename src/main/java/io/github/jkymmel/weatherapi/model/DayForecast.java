package io.github.jkymmel.weatherapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class DayForecast {
    private BigDecimal maxTemperature;
    private BigDecimal minTemperature;
    private LocalDate date;
}
