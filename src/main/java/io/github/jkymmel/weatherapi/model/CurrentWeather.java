package io.github.jkymmel.weatherapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class CurrentWeather {
    private Coordinates coordinates;
    private BigDecimal temperature;
}