package io.github.jkymmel.weatherapi.model;

import lombok.Getter;

@Getter
public class Coordinates {
    private double latitude;
    private double longitude;

    public Coordinates(double latitude, double longitude) {
        if (latitude <= 90.0d && latitude >= -90.0d && longitude <= 180.0d && longitude >= -180.0d) {
            this.latitude = latitude;
            this.longitude = longitude;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
