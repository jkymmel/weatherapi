package io.github.jkymmel.weatherapi.service;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import io.github.jkymmel.weatherapi.model.CurrentWeather;
import io.github.jkymmel.weatherapi.model.DayForecast;
import io.github.jkymmel.weatherapi.serialization.CurrentWeatherDeserializer;
import io.github.jkymmel.weatherapi.util.DayForecastListMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;

public class OpenWeatherMapService {

    private static final String API_KEY = System.getenv("OPENWEATHERMAP_API_KEY");

    private GsonBuilder gsonBuilder;

    public OpenWeatherMapService() {
        gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(CurrentWeather.class, new CurrentWeatherDeserializer());
    }

    public CurrentWeather getCurrentWeather(String cityName) throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("https://api.openweathermap.org/data/2.5/weather?q="
                + cityName + "&units=metric&APPID=" + API_KEY);
        HttpResponse response = httpClient.execute(request);
        String json = EntityUtils.toString(response.getEntity());
        return gsonBuilder.create().fromJson(json, CurrentWeather.class);
    }

    public List<DayForecast> getForecast(String cityName) throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("https://api.openweathermap.org/data/2.5/forecast?q="
                + cityName + "&units=metric&APPID=" + API_KEY);
        HttpResponse response = httpClient.execute(request);
        String json = EntityUtils.toString(response.getEntity());
        DayForecastListMapper dayForecastListMapper = new DayForecastListMapper();
        return dayForecastListMapper.map(json);
    }
}
