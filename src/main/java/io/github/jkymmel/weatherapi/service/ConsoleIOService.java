package io.github.jkymmel.weatherapi.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleIOService extends IOService {
    @Override
    public List<String> getCities() throws IOException {
        List<String> cities = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("Enter city or leave blank to start the process: ");
            String city = scanner.nextLine();
            if (city.equals("")) {
                return cities;
            } else {
                cities.add(city);
            }
        }
    }
}
