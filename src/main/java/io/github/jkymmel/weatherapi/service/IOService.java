package io.github.jkymmel.weatherapi.service;

import io.github.jkymmel.weatherapi.model.WeatherReport;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class IOService {
    public List<String> getCities() throws IOException {
        return Files.readAllLines(Paths.get("input.txt"));
    }

    public void exportCities(List<WeatherReport> weatherReports) {
        weatherReports.forEach(weatherReport -> {
            try {
                Files.write(Paths.get("output_" + weatherReport.getCity() + ".txt"), weatherReport.getPrintableString().getBytes());
            } catch (IOException e) {
                System.err.println("Failed to write output_" + weatherReport.getCity() + ".txt");
            }
        });
    }
}
