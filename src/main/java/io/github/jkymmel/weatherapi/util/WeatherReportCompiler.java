package io.github.jkymmel.weatherapi.util;

import io.github.jkymmel.weatherapi.model.CurrentWeather;
import io.github.jkymmel.weatherapi.model.DayForecast;
import io.github.jkymmel.weatherapi.model.WeatherReport;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class WeatherReportCompiler {
    public WeatherReport compile(String city, CurrentWeather currentWeather, List<DayForecast> dayForecastList) {
        List<DayForecast> threeDaysForecast = IntStream.range(0, 3).mapToObj(dayForecastList::get).collect(Collectors.toList());
        return new WeatherReport(city, currentWeather.getCoordinates(), currentWeather.getTemperature(), threeDaysForecast);
    }
}
