package io.github.jkymmel.weatherapi.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import io.github.jkymmel.weatherapi.model.DayForecast;
import io.github.jkymmel.weatherapi.model.ForecastEntity;
import io.github.jkymmel.weatherapi.serialization.ForecastEntityDeserializer;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DayForecastListMapper {
    private GsonBuilder gsonBuilder;

    public DayForecastListMapper() {
        gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ForecastEntity.class, new ForecastEntityDeserializer());
    }

    public List<DayForecast> map(String json) {
        JsonParser jsonParser = new JsonParser();
        Gson gson = gsonBuilder.create();
        Type type = new TypeToken<List<String>>(){}.getType();
        Map<LocalDate, List<ForecastEntity>> forecasts = StreamSupport.stream(jsonParser.parse(json)
                                .getAsJsonObject()
                                .get("list")
                                .getAsJsonArray()
                                .spliterator(), false)
                .map(s -> gson.fromJson(s, ForecastEntity.class))
                .collect(Collectors.groupingBy(ForecastEntity::getDate));
        return new DayForecastExtractor().extract(forecasts);
    }
}
