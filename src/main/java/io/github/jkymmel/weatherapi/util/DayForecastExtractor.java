package io.github.jkymmel.weatherapi.util;

import io.github.jkymmel.weatherapi.model.DayForecast;
import io.github.jkymmel.weatherapi.model.ForecastEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DayForecastExtractor {
    public List<DayForecast> extract(Map<LocalDate, List<ForecastEntity>> map) {
        return map.entrySet().stream()
                .map(e -> {
                    return new DayForecast(e.getValue().stream().map(ForecastEntity::getTemperature).max(Comparator.naturalOrder()).orElse(null),
                            e.getValue().stream().map(ForecastEntity::getTemperature).min(Comparator.naturalOrder()).orElse(null),
                            e.getKey());
                }).collect(Collectors.toList());
    }
}
