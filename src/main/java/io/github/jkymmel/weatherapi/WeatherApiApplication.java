package io.github.jkymmel.weatherapi;

import io.github.jkymmel.weatherapi.controller.WeatherController;
import io.github.jkymmel.weatherapi.service.ConsoleIOService;
import io.github.jkymmel.weatherapi.service.IOService;
import io.github.jkymmel.weatherapi.service.OpenWeatherMapService;
import io.github.jkymmel.weatherapi.util.WeatherReportCompiler;

import java.io.Console;
import java.io.IOException;
import java.util.Scanner;

public class WeatherApiApplication {
    public static void main(String[] args) throws IOException {
        System.out.println("Use console input instead on input.txt? [y/N]: ");
        Scanner scanner = new Scanner(System.in);
        String result = scanner.nextLine();
        IOService ioService = result.toLowerCase().equals("y") ? new ConsoleIOService() : new IOService();
        WeatherController weatherController = new WeatherController(new OpenWeatherMapService(), ioService, new WeatherReportCompiler());
        weatherController.run();
    }
}
