package io.github.jkymmel.weatherapi.serialization;

import com.google.gson.*;
import io.github.jkymmel.weatherapi.model.Coordinates;
import io.github.jkymmel.weatherapi.model.CurrentWeather;

import java.lang.reflect.Type;

public class CurrentWeatherDeserializer implements JsonDeserializer<CurrentWeather> {
    @Override
    public CurrentWeather deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        Coordinates coordinates = new Coordinates(jsonObject.getAsJsonObject("coord").get("lat").getAsDouble(),
                                                  jsonObject.getAsJsonObject("coord").get("lon").getAsDouble());
        return new CurrentWeather(coordinates,
                                  jsonObject.getAsJsonObject("main").get("temp").getAsBigDecimal());
    }
}
