package io.github.jkymmel.weatherapi.serialization;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import io.github.jkymmel.weatherapi.model.DayForecast;
import io.github.jkymmel.weatherapi.model.ForecastEntity;

import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

public class ForecastEntityDeserializer implements JsonDeserializer<ForecastEntity> {
    @Override
    public ForecastEntity deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return new ForecastEntity(
                jsonElement.getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsBigDecimal(),
                Instant.ofEpochSecond(jsonElement.getAsJsonObject().get("dt").getAsLong()).atZone(ZoneId.systemDefault()).toLocalDate()
        );
    }
}
